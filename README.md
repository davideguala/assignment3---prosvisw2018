# Assignment3 - HOW TO USE

### **Team**

Gualandris Davide, matricola 806938

### **Installazione**

##### **Pre-operazioni**

_Il progetto è stato sviluppato in un ambiente con S.O. Windows 10 Home._

1. Installare Eclipse, _è stata utilizzata la versione reperibile al seguente link: https://www.eclipse.org/downloads/download.php?file=/oomph/epp/2018-09/Ra/eclipse-inst-win64.exe_.
2. Installare MySql, _è stata utilizzata la versione MySQL Server 5.7_.
	* Creare una connessione a **localhost** con porta predefinita (_3306_) con username **root** senza password.
	* Altrimenti, modificare il file "\src\main\java\META-INF\persistence.xml" modificando il campo **value** alle righe 11 e 12 inserendo rispettivamente lo username e la password. 
3. Scaricare la presente Repository.
4. Importare il progetto in Eclipse come **Maven -> Existing Maven Project**.

##### **Esecuzione test**

Sono stati scritti 4 test, uno per ogni entità. Andando nel package "\src\test\java\test" saranno disponibili all'esecuzione come JUnit test.
Per eseguire un test, aprire la classe corrispondente al test a cui si è interessati, cliccare con il tasto destro sulla pagina e selezionare **Run As → JUnit Test**.

##### **Utilizzo GUI**

_Se si vuole utilizzare la GUI è necessario scaricare un Web Server per gestire le Servlet. Più precisamente è stato utilizzato TomCat v.9.0.14._

1. Scaricare TomCat, _reperibile al seguente link: https://tomcat.apache.org/download-90.cgi_.
2. Decomprimere lo zip in una cartella a proprio piacimento (e.g. Z:\apache-tomcat-9.0.14).
3. Configurare il server:
	1. Tasto destro sulla cartella del progetto nella finestra **Project Explorer**.
	2. **Properties → Targeted Runtimes → New → Apache Tomcat v9.0 → Browse** e selezionare la cartella in cui si trova TomCat (e.g. Z:\apache-tomcat-9.0.14).
	3. **Finish**.
	4. Selezionare Apache Tomcat e **Apply**.
4. Deployment del progetto:
	1. Tasto destro sulla cartella del progetto nella finestra **Project Explorer**.
	2. **Run As → Run on Server → Apache Tomcat → Finish**.
	3. Aprire la perspective Java EE: **Window → Open Perspective**.
	4. Nella perspective aperta spostarsi sulla Tab **Servers**, cliccare con il tasto destro sul server e selezionare **Start**.
5. Una volta compilato il codice, il progetto è reperibile al link **http://localhost:8080/assignment3**.