package actions;

import java.util.List;
import com.opensymphony.xwork2.ActionSupport;
import entities.Merch;
import services.ArtistService;
import services.ServiceInterface;
import services.MerchService;
import entities.Artist;

public class MerchAction extends ActionSupport {
	
	private static final long serialVersionUID = 1L;
	private ServiceInterface<Merch> merchService = new MerchService();
	private ServiceInterface<Artist> artistService = new ArtistService();
	private Merch merch;
	private Long id;
	private List<Merch> merchList;
	private List<Artist> artistList = artistService.getAll();
	private Long idArtist;
	private Long selectedArtist;
	private boolean error;
	
	//getters and setters
	
	public boolean isError() {
		return error;
	}

	public void setError(boolean error) {
		this.error = error;
	}

	public Merch getMerch() {
		return merch;
	}

	public void setMerch(Merch merch) {
		this.merch = merch;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Merch> getMerchList() {
		return merchList;
	}

	public void setMerchList(List<Merch> merchList) {
		this.merchList = merchList;
	}

	public List<Artist> getArtistList() {
		return artistList;
	}

	public void setArtistList(List<Artist> artistList) {
		this.artistList = artistList;
	}

	public Long getIdArtist() {
		return idArtist;
	}

	public void setIdArtist(Long idArtist) {
		this.idArtist = idArtist;
	}

	public Long getSelectedArtist() {
		return selectedArtist;
	}

	public void setSelectedArtist(Long selectedArtist) {
		this.selectedArtist = selectedArtist;
	}

	//CRUD operations
	
	public void validate(){
		try {
		    if (merch.getDescription().length() == 0) {
		        addFieldError("merch.description", "La descrizione � obbligatoria");
		    }
		    
		    if (merch.getDescription().length() > 45) {
		        addFieldError("merch.description", "Massimo 45 caratteri nella descrizione");
		    }
		    
		    if (merch.getTitle().length() == 0) {
		        addFieldError("merch.title", "Il titolo � obbligatorio");
		    }
		    
		    if (merch.getTitle().length() > 20) {
		        addFieldError("merch.title", "Massimo 20 caratteri nel titolo");
		    }
		
		    if (merch.getNumber() <= 0) {
		        addFieldError("merch.number", "Deve essere maggiore o uguale a 0");
		    }
		    
		    if (idArtist == 0) {
		        addFieldError("idArtist", "Devi scegliere un artista valido");
		    }
		    
		}catch(Exception e) {
			if(merch != null) {
				addFieldError("merch.title", "La compilazione dei campi non � corretta.");
				artistList = artistService.getAll();
			}			
		}
    }

	public String execute() {
		return SUCCESS;		
	}
	
	public String view() {
		merchList = merchService.getAll();
		return SUCCESS;
	}
	
	public String create() {
		merch = new Merch();
		artistList = artistService.getAll();
		selectedArtist = 0L;
		
		Artist fakeArtist = new Artist();
		fakeArtist.setId(0L);
		try {
			fakeArtist.setNickName("Seleziona un artista");
		} catch (Exception e) {
			e.printStackTrace();
		}
		artistList.add(fakeArtist);
		
		return INPUT;
	}
	
	public String saveOrUpdate() {
		
		merch.setArtist(artistService.get(idArtist));
		
		try {
		
			if (merch.getId() > -1L) {
				merchService.edit(merch);
	        } else {
	        	merchService.add(merch);
	        }
		
		}catch (Exception e) {
			addFieldError("merch.title", "La compilazione dei campi non � corretta.");
			artistList = artistService.getAll();
			return INPUT;
		}

		merchList = merchService.getAll();
		artistList = artistService.getAll();
        
        return SUCCESS;
    }
	
	public String edit() {
		
		merch = merchService.get(id);	
		artistList = artistService.getAll();
		merchList = merchService.getAll();
		
		Artist fakeArtist = new Artist();
		fakeArtist.setId(0L);
		try {
			fakeArtist.setNickName("Seleziona un artista");
		} catch (Exception e) {
			e.printStackTrace();
		}
		artistList.add(fakeArtist);
		
		selectedArtist = merch.getArtist().getId();
		
        return INPUT;		
	}
	
	public String delete() {
		
		try {
			merchService.delete(merchService.get(id));
		}catch (Exception e) {
			this.error= true;
		}
		merchList = merchService.getAll();

        return SUCCESS;
		
	}
}