package actions;

import java.util.Date;
import java.util.List;
import com.opensymphony.xwork2.ActionSupport;
import entities.Major;
import services.ArtistService;
import services.MajorService;
import services.ServiceInterface;
import entities.Artist;

public class ArtistAction extends ActionSupport {
	
	private static final long serialVersionUID = 1L;
	private ServiceInterface<Artist> artistService = new ArtistService(); //to handle the artists
	private ServiceInterface<Major> majorService = new MajorService();
	private Artist artist;
	private Long id;
	private List<Artist> artistList;
	private List<Major> majorList = majorService.getAll();
	private Long idMajor;
	private Long selectedMajor;
	private Major major;
	private boolean error;
	
	//getters and setters
	
	public Major getMajor() {
		return major;
	}

	public void setMajor(Major major) {
		this.major = major;
	}

	public boolean isError() {
		return error;
	}

	public void setError(boolean error) {
		this.error = error;
	}
	
	public Artist getArtist() {
		return artist;
	}

	public void setArtist(Artist artist) {
		this.artist = artist;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Artist> getArtistList() {
		return artistList;
	}

	public void setArtistList(List<Artist> artistList) {
		this.artistList = artistList;
	}

	public List<Major> getMajorList() {
		return majorList;
	}

	public void setMajorList(List<Major> majorList) {
		this.majorList = majorList;
	}

	public Long getIdMajor() {
		return idMajor;
	}

	public void setIdMajor(Long idMajor) {
		this.idMajor = idMajor;
	}
	
	public Long getSelectedMajor() {
		return selectedMajor;
	}

	public void setSelectedMajor(Long selectedMajor) {
		this.selectedMajor = selectedMajor;
	}

	//CRUD operations
	
	public void validate(){
		try {
	        if (artist.getRealName().length() == 0) {
	            addFieldError("artist.realName", "Il nome reale � obbligatorio");
	        }
	        
	        if (artist.getRealName().length() > 45) {
	            addFieldError("artist.realName", "Massimo 45 caratteri nel nome reale");
	        }
	
	        if (artist.getNickName().length() == 0) {
	            addFieldError("artist.nickName", "Il nome d'arte � obbligatorio.");
	        }
	        
	        if (artist.getNickName().length() > 45) {
	            addFieldError("artist.nickName", "Massimo 45 caratteri nel nome d'arte");
	        }
	        
	        if (artist.getBirthDate() == null) {
	            addFieldError("artist.birthDate", "La data � obbligatoria");
	        }
	        
	        if (artist.getBirthDate().after(new Date())) {
	            addFieldError("artist.birthDate", "La data deve essere antecedente o uguale a oggi");
	        }
	        
		}catch(Exception e) {
			if(artist != null) {
				addFieldError("artist.realName", "La compilazione dei campi non � corretta.");
				majorList = majorService.getAll();
			}			
		}
    }
	
	public String execute() {
		return SUCCESS;		
	}
	
	public String view() {
		artistList = artistService.getAll();
		return SUCCESS;
	}
	
	public String create() {		
		artist = new Artist();
		majorList = majorService.getAll();
		
		Major fakeMajor = new Major();
		fakeMajor.setId(0L);
		try {
			fakeMajor.setName("Scegli una major");
		} catch (Exception e) {
			e.printStackTrace();
		}		
		majorList.add(fakeMajor);
		
		selectedMajor = 0L;
		return INPUT;
	}
	
	public String saveOrUpdate() {
		
		majorService.get(idMajor).getArtists().add(artist);
		
		try {
			if (artist.getId() > -1L) {
	        	artistService.edit(artist);
	        } else {
	        	artistService.add(artist);
	        }
		}catch (Exception e) {
			addFieldError("artist.realName", "La compilazione dei campi non � corretta.");
			majorList = majorService.getAll();
		}

        artistList = artistService.getAll();
        
        return SUCCESS;
    }
	
	public String edit() {
		
		artist = artistService.get(id);	
		majorList = majorService.getAll();
		selectedMajor = 0L;
		
		for(Major m : majorList)
			if(m.getArtists().contains(artist)) {
				selectedMajor = m.getId();
				break;
			}
		
		Major fakeMajor = new Major();
		fakeMajor.setId(0L);
		try {
			fakeMajor.setName("Scegli una major");
		} catch (Exception e) {
			e.printStackTrace();
		}		
		majorList.add(fakeMajor);
		
        return INPUT;		
	}
	
	public String delete() {
		
		try {
			artistService.delete(artistService.get(id));
		}catch (Exception e) {
			this.error= true;
		}
        artistList = artistService.getAll();

        return SUCCESS;
		
	}
	
}