package actions;

import java.util.ArrayList;
import java.util.List;
import com.opensymphony.xwork2.ActionSupport;
import entities.Album;
import entities.Artist;
import services.AlbumService;
import services.ArtistService;
import services.ServiceInterface;

public class AlbumAction extends ActionSupport{
	
	private static final long serialVersionUID = 1L;
	private ServiceInterface<Artist> artistService = new ArtistService(); //to handle the artists
	private ServiceInterface<Album> albumService = new AlbumService();
	private Album album;
	private Long id;
	private List<Album> albumList;
	private List<Artist> artistList = artistService.getAll();
	private List<Long> idArtists;
	private boolean error;
	
	//getters and setters
	
	public boolean isError() {
		return error;
	}

	public void setError(boolean error) {
		this.error = error;
	}
	
	public Album getAlbum() {
		return album;
	}

	public void setAlbum(Album album) {
		this.album = album;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Album> getAlbumList() {
		return albumList;
	}

	public void setAlbumList(List<Album> albumList) {
		this.albumList = albumList;
	}

	public List<Artist> getArtistList() {
		return artistList;
	}

	public void setArtistList(List<Artist> artistList) {
		this.artistList = artistList;
	}

	public List<Long> getIdArtists() {
		return idArtists;
	}

	public void setIdArtists(List<Long> idArtists) {
		this.idArtists = idArtists;
	}

	//CRUD operations
	
	public void validate(){
		try {
	        if (album.getName().length() == 0) {
	            addFieldError("album.name", "Il nome � obbligatorio");
	        }
	        
	        if (album.getName().length() > 45) {
	            addFieldError("album.name", "Massimo 45 caratteri nel nome dell'album");
	        }
	        
	        if (idArtists.size() == 0) {
	            addFieldError("album.name", "Devi scegliere almeno un'artista");
	        }
	        
	        
		}catch(Exception e) {
			if(album != null) {
				addFieldError("album.name", "La compilazione dei campi non � corretta.");
				artistList = artistService.getAll();
			}			
		}
    }
	
	public String execute() {
		return SUCCESS;		
	}
	
	public String view() {
		albumList = albumService.getAll();
		return SUCCESS;
	}
	
	public String create() {		
		album = new Album();
		artistList = artistService.getAll();
		return INPUT;
	}
	
	public String saveOrUpdate() {
		
		album.setArtists(new ArrayList<Artist>());
		
		for (Long id : idArtists) {
			
			Artist a = artistService.get(id);
			if(a.getAlbums().contains(album))
				a.getAlbums().remove(album);
			
			album.addArtist(a);
		}
		
		try {
			if (album.getId() > -1L) {
				albumService.edit(album);
	        } else {
	        	albumService.add(album);
	        }
		}catch (Exception e) {
			addFieldError("album.name", "La compilazione dei campi non � corretta.");
			artistList = artistService.getAll();
		}

        albumList = albumService.getAll();
		
        return SUCCESS;
    }
	
	public String edit() {
		
		album = albumService.get(id);
		artistList = artistService.getAll();
		
        return INPUT;		
	}
	
	public String delete() {
		
		try {
			albumService.delete(albumService.get(id));
		}catch (Exception e) {
			this.error= true; 
		}

		albumList = albumService.getAll();

        return SUCCESS;
		
	}
}
