package actions;

import java.util.Date;
import java.util.List;
import com.opensymphony.xwork2.ActionSupport;
import entities.Major;
import services.MajorService;
import services.ServiceInterface;

public class MajorAction extends ActionSupport {
	private static final long serialVersionUID = 1L;
	private ServiceInterface<Major> majorService = new MajorService();
	private Major major;
	private Long id;
	private List<Major> majorList;
	private Long idMajor;
	private Long selectedMajor;
	private boolean error;
	
	//getters and setters
	
	public boolean isError() {
		return error;
	}

	public void setError(boolean error) {
		this.error = error;
	}
	
	public Major getMajor() {
		return major;
	}

	public void setMajor(Major major) {
		this.major = major;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdMajor() {
		return idMajor;
	}

	public void setIdMajor(Long idMajor) {
		this.idMajor = idMajor;
	}

	public Long getSelectedMajor() {
		return selectedMajor;
	}

	public void setSelectedMajor(Long selectedMajor) {
		this.selectedMajor = selectedMajor;
	}

	public List<Major> getMajorList() {
		return majorList;
	}

	public void setMajorList(List<Major> majorList) {
		this.majorList = majorList;
	}

	//CRUD operations
	
	public void validate(){
		try {
	        if (major.getName().length() == 0) {
	            addFieldError("major.name", "Il nome � obbligatorio.");
	        }
	        
	        if (major.getName().length() > 45) {
	            addFieldError("major.name", "Massimo 45 caratteri nel nome");
	        }
	
	        if (major.getFoundation() == null) {
	            addFieldError("major.foundation", "La data � obbligatoria");
	        }
	        
	        if (major.getFoundation().after(new Date())) {
	            addFieldError("major.foundation", "La data deve essere antecedente o uguale a oggi");
	        }
	        
		}catch(Exception e) {
			if(major != null) {
				addFieldError("major.name", "La compilazione dei campi non � corretta.");
				majorList = majorService.getAll();
			}			
		}
    }
	
	public String execute() {
		return SUCCESS;		
	}
	
	public String view() {
		majorList = majorService.getAll();
		return SUCCESS;
	}
	
	public String create() {		
		major = new Major();
		majorList = majorService.getAll();
		selectedMajor = 0L;
		
		Major fakeMajor = new Major();
		fakeMajor.setId(0L);
		try {
			fakeMajor.setName("Seleziona una major");
		} catch (Exception e) {
			e.printStackTrace();
		}
		majorList.add(fakeMajor);

		return INPUT;
	}
	
	public String saveOrUpdate() {
		
		major.setOwnedBy(majorService.get(idMajor));
		try {
			if (major.getId() > -1L) {
	        	majorService.edit(major);
	        } else {
	        	majorService.add(major);
	        }
		}catch (Exception e) {
			addFieldError("major.name", "La compilazione dei campi non � corretta.");
			majorList = majorService.getAll();
		}

        majorList = majorService.getAll();
        
        return SUCCESS;
    }
	
	public String edit() {		
		major = majorService.get(id);		
		majorList = majorService.getAll();
		selectedMajor = 0L;
		
		Major fakeMajor = new Major();
		fakeMajor.setId(0L);
		try {
			fakeMajor.setName("Seleziona una major");
		} catch (Exception e) {
			e.printStackTrace();
		}
		majorList.add(fakeMajor);
		
		if(major.getOwnedBy() != null) {
			selectedMajor = major.getOwnedBy().getId();
		}else {
			selectedMajor = 0L;
		}
		
        return INPUT;		
	}
	
	public String delete() {
		try {
			majorService.delete(majorService.get(id));
		}catch (Exception e) {
			this.error= true;
		}
		
        majorList = majorService.getAll();
        return SUCCESS;	
	}
	
}