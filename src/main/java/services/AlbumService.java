package services;

import java.util.List;
import entities.Album;
import daos.AlbumDao;
/*import java.util.Iterator;
 * import daos.ArtistDao;
 * import entities.Artist;
 */

public class AlbumService implements ServiceInterface<Album>{

	private AlbumDao albumDao;
	
	public AlbumService() {
        this.albumDao = new AlbumDao();
    }
	
	public Album get(Long id) {		
		albumDao.beginTransaction();
		Album a = albumDao.get(id);
		albumDao.commitTransaction();		
		return a;
	}

	public void add(Album a) throws Exception {		
		albumDao.beginTransaction();
		try {
			albumDao.add(a);
			albumDao.commitTransaction();
		}catch (Exception e) {
			albumDao.rollbackTransaction();
			throw e;
		}           
        return;
	}	
	
	public List<Album> getAll(){
		albumDao.beginTransaction();
        List<Album> albums = albumDao.getAll();
        albumDao.commitTransaction();
        return albums;
	}
	
	public void delete(Album a) throws Exception {		
        albumDao.beginTransaction();
        
        try {
        	
        	for(entities.Artist art: a.getArtists())
        		if(art.getAlbums().contains(a))
        			art.getAlbums().remove(a);
        	
			albumDao.delete(a);
			albumDao.commitTransaction();
		}catch (Exception e) {
			albumDao.rollbackTransaction();
			throw e;
		}           

        return;
	}
	
	public void edit(Album a) throws Exception {		
		albumDao.beginTransaction();
		try {
			albumDao.edit(a);
			albumDao.commitTransaction();
		}catch (Exception e) {
			albumDao.rollbackTransaction();
			throw e;
		}   
        
        return;
	}
	
	public List<Album> search(String field, String value){
		albumDao.beginTransaction();
        List<Album> results = albumDao.search(field, value);
        albumDao.commitTransaction();
        return results;
	}

}
