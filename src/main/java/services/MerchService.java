package services;

import java.util.List;
import daos.MerchDao;
import entities.Merch;

public class MerchService implements ServiceInterface<Merch>{

	private MerchDao merchDao;
	
	public MerchService() {
        this.merchDao = new MerchDao();
    }
	
	public Merch get(Long id) {		
		merchDao.beginTransaction();
		Merch m = merchDao.get(id);
		merchDao.commitTransaction();		
		return m;
	}

	public void add(Merch m) throws Exception {		
		merchDao.beginTransaction();
		try {
			merchDao.add(m);
			merchDao.commitTransaction();
		}catch (Exception e) {
			merchDao.rollbackTransaction();
			throw e;
		}        
        return;
	}	
	
	public List<Merch> getAll(){
		merchDao.beginTransaction();
        List<Merch> merch = merchDao.getAll();
        merchDao.commitTransaction();
        return merch;
	}
	
	public void delete(Merch m) throws Exception {
		merchDao.beginTransaction();
		try {
			m.getArtist().getMerch().remove(m);	
			merchDao.delete(m);
			merchDao.commitTransaction();
		}catch (Exception e) {
			merchDao.rollbackTransaction();
			throw e;
		}        
        return;
	}
	
	public void edit(Merch m) throws Exception {		
		merchDao.beginTransaction();
		try {
			merchDao.edit(m);
			merchDao.commitTransaction();
		}catch (Exception e) {
			merchDao.rollbackTransaction();
			throw e;
		}
        
        return;
	}
	
	public List<Merch> search(String field, String value){
		merchDao.beginTransaction();
        List<Merch> results = merchDao.search(field, value);
        merchDao.commitTransaction();
        return results;
	}
}
