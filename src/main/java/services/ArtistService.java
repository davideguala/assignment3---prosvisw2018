package services;

import java.util.List;
import daos.ArtistDao;
import entities.Artist;

public class ArtistService implements ServiceInterface<Artist>{

	private ArtistDao artistDao;
	
	public ArtistService() {
        this.artistDao = new ArtistDao();
    }
	
	public Artist get(Long id) {
		artistDao.beginTransaction();
		Artist a = artistDao.get(id);
		artistDao.commitTransaction();		
		return a;
	}

	public void add(Artist a) throws Exception {
		artistDao.beginTransaction();
		try {
			artistDao.add(a);
	        artistDao.commitTransaction();
		}catch (Exception e) {
			artistDao.rollbackTransaction();
			throw e;
		}        
        return;
	}	

	public List<Artist> getAll(){
		artistDao.beginTransaction();
        List<Artist> artists = artistDao.getAll();
        artistDao.commitTransaction();
        return artists;
	}
	
	public void delete(Artist a) throws Exception {
		for(entities.Major m: new MajorService().getAll())
			if(m.getArtists().contains(a))
				m.getArtists().remove(a);
		artistDao.beginTransaction();
		try {
			artistDao.delete(a);
			artistDao.commitTransaction();
		}catch (Exception e) {
			artistDao.rollbackTransaction();
			throw e;
		}           
	}
	
	public void edit(Artist a) throws Exception {		
		artistDao.beginTransaction();
		try {
			artistDao.edit(a);
			artistDao.commitTransaction();
		}catch (Exception e) {
			artistDao.rollbackTransaction();
			throw e;
		}   
        
        return;
	}

	public List<Artist> search(String field, String value){
		artistDao.beginTransaction();
        List<Artist> results = artistDao.search(field, value);
        artistDao.commitTransaction();
        return results;
	}
}
