package services;

import java.util.List;
import daos.MajorDao;
import entities.Major;

public class MajorService implements ServiceInterface<Major>{

	private MajorDao majorDao;
	
	public MajorService() {
        this.majorDao = new MajorDao();
    }
	
	public Major get(Long id) {		
		majorDao.beginTransaction();
		Major m = majorDao.get(id);
		majorDao.commitTransaction();		
		return m;
	}

	public void add(Major m) throws Exception {		
		majorDao.beginTransaction();
		try {
			majorDao.add(m);
			majorDao.commitTransaction();
		}catch (Exception e) {
			majorDao.rollbackTransaction();
			throw e;
		}
    }	
	
	public List<Major> getAll(){
		majorDao.beginTransaction();
        List<Major> majors = majorDao.getAll();
        majorDao.commitTransaction();
        return majors;
    }
	
	public void delete(Major m) throws Exception{
		majorDao.beginTransaction();
		try {	
			if(m.getOwnedBy() != null)
				m.getOwnedBy().getMajorsOwned().remove(m);		
			for(Major m1: m.getMajorsOwned())
				m1.setOwnedBy(null);
						
			majorDao.delete(m);
			majorDao.commitTransaction();
		}catch (Exception e) {
			majorDao.rollbackTransaction();
			throw e;
		}        
        return;
	}
	
	public void edit(Major m) throws Exception {		
		majorDao.beginTransaction();
		try {
			majorDao.edit(m);
			majorDao.commitTransaction();
		}catch (Exception e) {
			majorDao.rollbackTransaction();
			throw e;
		}        
        return;
	}
	
	public List<Major> search(String field, String value){
		majorDao.beginTransaction();
        List<Major> results = majorDao.search(field, value);
        majorDao.commitTransaction();
        return results;
	}
	
}
