package services;

import java.util.List;

public interface ServiceInterface<T> {
	List<T> getAll();
    T get(Long id);
    void add(T t) throws Exception;
    void delete(T t) throws Exception;
    void edit(T t) throws Exception;
    List<T> search(String field, String value);
}
