package entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name=Merch.tableName)
public class Merch {
	public static final String tableName = "Merch";
	
	//fields

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="merch_id")
	private Long id;
	
	@Column(nullable=false, unique = true, length = 45)
    private String title;
	
	@Column(nullable=false, length = 45)
    private String description;
    
	@Column(nullable=false)
    private int number;
    
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="idArtist", referencedColumnName="artist_id")
    private Artist artist;

	//constructors
	
	public Merch() {
		this.id = -1L;
	}

	//getters and setters
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) throws Exception {
		if(title.length() == 0) {
			throw new Exception("No empty string");
		}
		if(title.length() > 20) {
			throw new Exception("Maximum 20 character");
		}
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) throws Exception {
		if(description.length() == 0) {
			throw new Exception("No empty string");
		}
		if(description.length() > 45) {
			throw new Exception("Maximum 45 character");
		}
		this.description = description;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) throws Exception {
		if(number <= 0) {
			throw new Exception("Number must be bigger than zero");
		}
		this.number = number;
	}

	public Artist getArtist() {
		return artist;
	}

	public void setArtist(Artist artist) {
		if(this.artist != null)
			this.artist.getMerch().remove(this);
		this.artist = artist;
		artist.getMerch().add(this);
	}
	
	//override toString()

	@Override
	public String toString()
    {
        return "Id: " + getId() + " | " +
        " Titolo:  " + getTitle() + " | " +
        " Descrizione:  " + getDescription() + " | " +
        " In Magazzino: " + getNumber();
    }
	
	//override equals()
	
	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Merch m = (Merch) o;
        if(this.getId() == m.getId() && 
            	this.getTitle() == m.getTitle() &&
            	this.getDescription() == m.getDescription() &&
            	this.getNumber() == m.getNumber()) return true;
        return false;
    }
	
}
