package entities;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name=Major.tableName)
public class Major {
	public static final String tableName = "Major";

	//fields
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="major_id")
	private Long id;
	
	@Column(nullable=false, length = 45, unique = true)
    private String name;
	
	@Column(nullable=false)
    private Date foundation;
	
	@OneToMany(fetch = FetchType.LAZY)
	@JoinColumn(name="idMajor")
	private List<Artist> artists;
 
	//self-relantionship
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="idMajor", nullable=true, referencedColumnName="major_id")
	private Major ownedBy;

	@OneToMany(mappedBy="ownedBy", fetch = FetchType.LAZY)
	private List<Major> majorsOwned;
	
	//constructors
	
    public Major() {
    	this.id = -1L;
		this.artists = new ArrayList<Artist>();
		this.majorsOwned = new ArrayList<Major>();
	}
    
    //getters and setters
	
	public List<Artist> getArtists() {
		return artists;
	}


	public void setArtists(List<Artist> artists) {
		this.artists = artists;
	}
    
    public Major getOwnedBy() {
		return ownedBy;
	}

	public void setOwnedBy(Major ownedBy) {
		this.ownedBy = ownedBy;
		if(ownedBy != null)
			ownedBy.getMajorsOwned().add(this);
	}

	public List<Major> getMajorsOwned() {
		return majorsOwned;
	}

	public void setMajorsOwned(List<Major> majorsOwned) {
		this.majorsOwned = majorsOwned;
		for(Major m : majorsOwned)
			m.setOwnedBy(this);
	}

	public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name) throws Exception{
    	if(name.length() == 0) {
			throw new Exception("No empty string");
		}
		if(name.length() > 45) {
			throw new Exception("Maximum 45 character");
		}
        this.name = name;
    }

	public void setFoundation(Date foundation) throws Exception {
		if (foundation == null) {
			throw new Exception("No empty date");
        }        
        if (foundation.after(new Date())) {
        	throw new Exception("Date must be before than today");
        }
		this.foundation = foundation;
	}

	public Date getFoundation() {
		return foundation;
	}
	
	//override toString()
	
	@Override
	public String toString()
    {
        return "Id: " + getId() + " | " +
        " Nome:  " + getName() + " | " +
        " Data di fondazione: " + getFoundation();
    }
	
	//override equals()
	
	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Major m = (Major) o;
        if(this.getId() == m.getId() && 
            	this.getName() == m.getName() &&
            	this.getFoundation() == m.getFoundation()) return true;
        return false;

    }
	
}
