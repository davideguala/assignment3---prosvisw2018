package entities;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name=Artist.tableName)
public class Artist {
	public static final String tableName = "Artist";
	
	//fields
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="artist_id")
	private Long id;
	
	@Column(nullable=false, length = 45)
    private String realName;
    
	@Column(nullable=false, length = 45)
    private String nickName;
    
	@Column(nullable=false)
    private Date birthDate;
	
	@ManyToMany(cascade = {CascadeType.DETACH, CascadeType.REMOVE}, mappedBy = "artists", fetch = FetchType.LAZY)
	private List<Album> albums;
	
	@OneToMany(cascade = {CascadeType.DETACH, CascadeType.REMOVE}, mappedBy = "artist", fetch = FetchType.LAZY)
	private List<Merch> merch;
	
	//constructors
	
	public Artist() {
		this.id = -1L;
		this.albums = new ArrayList<Album>();
		this.merch = new ArrayList<Merch>();
	}

    //getters and setters
    
	public List<Merch> getMerch() {
		return merch;
	}

	public void setMerch(List<Merch> merch) {
		this.merch = merch;
		for(Merch m : merch)
			m.setArtist(this);
	}

	public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getRealName()
    {
        return realName;
    }

    public void setRealName(String name) throws Exception
    {
    	if(name.length() == 0) {
			throw new Exception("No empty string");
		}
		if(name.length() > 45) {
			throw new Exception("Maximum 45 character");
		}
        this.realName = name;
    }

	public void setBirthDate(Date birthdate) throws Exception {
		if (birthdate == null) {
			throw new Exception("No empty date");
        }        
        if (birthdate.after(new Date())) {
        	throw new Exception("Date must be before than today");
        }
		this.birthDate = birthdate;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public List<Album> getAlbums() {
		return albums;
	}

	public void setAlbums(List<Album> albums) {
		this.albums= albums;
	}

	public void setNickName(String nickname) throws Exception {
		if(nickname.length() == 0) {
			throw new Exception("No empty string");
		}
		if(nickname.length() > 45) {
			throw new Exception("Maximum 45 character");
		}
		this.nickName = nickname;
	}

	public String getNickName() {
		return nickName;
	}
	
	//override toString()
	
	@Override
	public String toString()
    {
        return "Id: " + getId() + " | " +
        " Nome:  " + getRealName() + " | " +
        " Nome d'arte: " + getNickName() + " | " +
        " Data di nascita: " + getBirthDate();
    }
	
	//override equals()
	
	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Artist a = (Artist) o;
        if(this.getId() == a.getId() && 
        	this.getNickName() == a.getNickName() &&
        	this.getRealName() == a.getRealName() &&
        	this.getBirthDate() == a.getBirthDate()) return true;
        return false;
    }
	
}
