package entities;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name=Album.tableName)
public class Album {
	public static final String tableName = "Album";

	//fields
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="album_id")
	private Long id;
	
	@Column(nullable=false, length = 45)
    private String name;
    
	@Column(nullable=false)
    private Date releaseDate;
	
	@ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "album_artist",
            joinColumns = @JoinColumn(name = "album_id"),
            inverseJoinColumns = @JoinColumn(name = "artist_id")
    )
	private List<Artist> artists;
	
	//constructors
	
	public Album() {
		this.id = -1L;
		this.artists = new ArrayList<Artist>();
	}

	//getters and setters
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) throws Exception {
		if(name.length() == 0) {
			throw new Exception("No empty string");
		}
		if(name.length() > 45) {
			throw new Exception("Maximum 45 character");
		}
		this.name = name;
	}

	public Date getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(Date releaseDate){
		this.releaseDate = releaseDate;
	}

	public List<Artist> getArtists() {
		return artists;
	}

	public void setArtists(List<Artist> artists) {
		this.artists = artists;
	}

	public void addArtist(Artist artist) {
        artists.add(artist);
        artist.getAlbums().add(this);
    }

    public void removeArtist(Artist artist) {
    	artists.remove(artist);
    	artist.getAlbums().remove(this);
    }
	
	//override toString()
	
	@Override
	public String toString()
    {
        return "Id: " + getId() + " | " +
        " Nome:  " + getName() + " | " +
        " Data d'uscita: " + getReleaseDate();
    }
	
	//override equals()
	
	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Album a = (Album) o;
        if(this.getId() == a.getId() && 
            	this.getName() == a.getName() &&
            	this.getReleaseDate() == a.getReleaseDate()) return true;
        return false;
    }
	
}
