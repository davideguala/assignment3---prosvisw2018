package daos;

import javax.persistence.EntityManager;
import java.util.List;

public abstract class DaoAbstract<T> {

    abstract List<T> getAll();
    abstract T get(Long id);
    abstract void add(T t);
    abstract void delete(T t);
    abstract void edit(T t);
    abstract List<T> search(String field, String value);

    public DaoAbstract() {}

    public void beginTransaction() {
        getEntityManager().getTransaction().begin();
    }

    public void commitTransaction() {
    	getEntityManager().flush();
    	getEntityManager().getTransaction().commit();
    }

    public EntityManager getEntityManager() {
        return EntityManagerSingleton.getEntityManager();
    }
    
    public void rollbackTransaction() {
    	getEntityManager().getTransaction().rollback();
    }
}
