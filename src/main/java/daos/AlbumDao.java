package daos;

import java.util.List;
import javax.persistence.EntityManager;
import entities.Album;

public class AlbumDao extends DaoAbstract<Album>{
	
	@Override
    public List<Album> getAll() {
        return getEntityManager().createQuery("from Album").getResultList();
    }

    @Override
    public Album get(Long id) {
        return getEntityManager().find(Album.class, id);
    }

    @Override
    public void add(Album a) {
        getEntityManager().persist(a);
        return;
    }

    @Override
    public void delete(Album a) {
    	EntityManager em = getEntityManager();
    	em.refresh(a);
		em.remove(a);
		return;
    }

    @Override
    public void edit(Album a) {
        getEntityManager().merge(a);
        return;
    }
    
    @Override
    public List<Album> search(String field, String value){
    	String query = "SELECT a FROM Album a WHERE a." + field + " LIKE '%"+value+"%'";
    	return getEntityManager().createQuery(query).getResultList();
    }
}
