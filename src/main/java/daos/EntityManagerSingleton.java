package daos;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

public class EntityManagerSingleton {
	
	private static EntityManager entityManager = null;

	private EntityManagerSingleton() {}
	
	public static EntityManager getEntityManager() {
		if(entityManager==null)
			entityManager =  Persistence.createEntityManagerFactory("dbMusic").createEntityManager();
		return entityManager;
	}
	
}
