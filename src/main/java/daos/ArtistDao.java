package daos;

import java.util.List;
import javax.persistence.EntityManager;
import entities.Artist;

public class ArtistDao extends DaoAbstract<Artist>{
	
	@Override
    public List<Artist> getAll() {
        return getEntityManager().createQuery("from Artist").getResultList();
    }

    @Override
    public Artist get(Long id) {
        return getEntityManager().find(Artist.class, id);
    }

    @Override
    public void add(Artist artist) {
        getEntityManager().persist(artist);
        return;
    }

    @Override
    public void delete(Artist a) {
    	EntityManager em = getEntityManager();
    	em.refresh(a);
    	em.merge(a);
		em.remove(a);
		return;
    }

    @Override
    public void edit(Artist a) {
        getEntityManager().merge(a);
        return;
    }
    
    @Override
    public List<Artist> search(String field, String value){
    	String query = "SELECT a FROM Artist a WHERE a." + field + " LIKE '%"+value+"%'";
    	return getEntityManager().createQuery(query).getResultList();
    }
}
