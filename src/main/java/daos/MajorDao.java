package daos;

import java.util.List;

import javax.persistence.EntityManager;

import entities.Major;

public class MajorDao extends DaoAbstract<Major>{
	
	
    public List<Major> getAll() {
        return getEntityManager().createQuery("SELECT m from Major m").getResultList();
    }

    @Override
    public Major get(Long id) {
        return getEntityManager().find(Major.class, id);
    }

    @Override
    public void add(Major major) {
        getEntityManager().persist(major);
        return;
    }

    @Override
    public void delete(Major m) {
    	EntityManager em = getEntityManager();
		//gestisce le foreign key
		em.createQuery("UPDATE Artist a SET idMajor = null WHERE idMajor = :did")
				.setParameter("did", m.getId()).executeUpdate();
    	em.createQuery("UPDATE Major m SET idMajor = null WHERE idMajor = :did")
			.setParameter("did", m.getId()).executeUpdate();
		em.remove(m);
		return;
    }

    @Override
    public void edit(Major major) {
        getEntityManager().merge(major);
        return;
    }
    
    @Override
    public List<Major> search(String field, String value){
    	String query = "SELECT m FROM Major m WHERE m." + field + " LIKE '%"+value+"%'";
    	return getEntityManager().createQuery(query).getResultList();
    }
}
