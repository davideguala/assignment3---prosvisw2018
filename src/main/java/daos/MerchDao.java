package daos;

import java.util.List;
import javax.persistence.EntityManager;
import entities.Merch;

public class MerchDao extends DaoAbstract<Merch>{
	
	@Override
    public List<Merch> getAll() {
        return getEntityManager().createQuery("from Merch").getResultList();
    }

    @Override
    public Merch get(Long id) {
        return getEntityManager().find(Merch.class, id);
    }

    @Override
    public void add(Merch m) {
        getEntityManager().persist(m);
        return;
    }

    @Override
    public void delete(Merch m) {
    	EntityManager em = getEntityManager();
		em.remove(m);
		return;
    }

    @Override
    public void edit(Merch m) {
        getEntityManager().merge(m);
        return;
    }
    
    @Override
    public List<Merch> search(String field, String value){
    	String query = "SELECT m FROM Merch m WHERE m." + field + " LIKE '%"+value+"%'";
    	return getEntityManager().createQuery(query).getResultList();
    }
}
