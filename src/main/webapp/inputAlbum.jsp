<%@ page language="java" pageEncoding="ISO-8859-1" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>Gestisci Album</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    

  </head>
  
  <body>
  <s:if test="id > 0">
    <h3>Modifica album</h3>
  </s:if>
  <s:else>
  	<h3>Crea album</h3>
  
  </s:else>
  
  <s:form action='saveOrUpdateAlbum'>
  
  	<p><s:textfield name="album.name" label="Nome album" /> </p> <br />
  	<p><s:textfield name="album.releaseDate" label="Data di uscita (dd/MM/yyyy)" /></p> <br/>
	<s:select label="Artisti compositori" name="idArtists" id="idArtists" 
      list="artistList"
      listKey="id"
      listValue="nickName"
      multiple="true" required="true"/>
    
  	<s:hidden name="album.id" />
  	<s:submit  value="Save" />
  
  </s:form>
   
  </body>
</html>
