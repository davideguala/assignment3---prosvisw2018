<%@ page language="java" pageEncoding="ISO-8859-1" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">    
    <title>Vedi album</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    

  </head>
  
  <body>
  <h3>Lista album</h3>
  <h4><a href="index.jsp">Torna alla Home</a></h4>
    
    <s:if test="error">
    	<h1>ERRORE</h1>
    </s:if>
    
    <s:if test="albumList.size > 0">
       <ol>
         <s:iterator value="albumList">
	  		<li>
	  			Nome:
	  			<s:property value="name" />
	  			<br/>
	  			
	  			Id album:
	  			<s:property value="id" />
	  			<br/>
	  			
	  			<s:if test="artists.size > 0">
					Artisti:
						<select multiple>
							<s:iterator value="artists">
								<option>
									<s:property value="nickName" />								
								</option>								
							</s:iterator>
						</select>	
						<br/>			
				</s:if>
	  			<br/>
	  			
	  			<s:if test="tracks.size > 0">
					Tracce dell'album:
						<select multiple>
							<s:iterator value="tracks">
								<option>
									<s:property value="title" />								
								</option>								
							</s:iterator>
						</select>	
						<br/>			
				</s:if>
				
  				<s:url action="editAlbum" var="editUrl">
				       <s:param name="id" value="id"/>
			    </s:url>		
                <s:url action="deleteAlbum" var="deleteUrl">
				       <s:param name="id" value="id"/>
			    </s:url>
			    
			    
	  			<a href="<s:property value='#editUrl' />" >Modifica</a>
	  			<br/>
	  			<a href="<s:property value='#deleteUrl' />" >Cancella</a>
	  			<br/><br/>
	  		</li>
		</s:iterator>
       </ol>
    </s:if>
   
	<s:url action="createAlbum" var="newUrl">
       <s:param name="id" value="-1"/>
     </s:url>
     
    <p><a href="<s:property value='#newUrl' />" >Crea un nuovo album.</a></p>
  </body>
</html>
