<%@ page language="java" pageEncoding="ISO-8859-1" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>Gestisci Major</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    

  </head>
  
  <body>
  <s:if test="id > 0">
    <h3>Modifica Major</h3>
  </s:if>
  <s:else>
  	<h3>Crea Major</h3>
  
  </s:else>
  
  <s:form action='saveOrUpdateMajor'>
  
  	<p><s:textfield name="major.name" label="Nome" /> <br />
  	<s:textfield name="major.foundation" label="Fondazione (dd/MM/yyyy)" /></p> <br/>
  	<s:hidden name="major.id" />
  	
  	<s:select label="Posseduta da ..." name="idMajor" id="idMajor" 
      list="majorList"
      listKey="id"
      listValue="name"
      value="selectedMajor"/>
  	
  	<s:submit value="Salva" />
  
  </s:form>
   
  </body>
</html>
