<%@ page language="java" pageEncoding="ISO-8859-1" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>Gestisci artista</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    

  </head>
  
  <body>
  <s:if test="id > 0">
    <h3>Modifica artista</h3>
  </s:if>
  <s:else>
  	<h3>Crea artista</h3>
  
  </s:else>
  
  <s:form action='saveOrUpdateArtist'>
  
  	<p><s:textfield name="artist.realName" label="Nome 'reale'" /> </p> <br />
  	<p><s:textfield name="artist.nickName" label="Nome d'arte" /> </p> <br />
  	<p><s:textfield name="artist.birthDate" label="Data di nascita (dd/MM/yyyy)" /></p> <br/>
	
	<s:select label="Major di appartenenza" name="idMajor" id="idMajor" 
      list="majorList"
      listKey="id"
      listValue="name"
      value="selectedMajor"/>
    
  	<s:hidden name="artist.id" />
  	<s:submit  value="Save" />
  
  </s:form>
   
  </body>
</html>
