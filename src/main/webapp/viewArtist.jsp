<%@ page language="java" pageEncoding="ISO-8859-1" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">    
    <title>Vedi artisti</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    

  </head>
  
  <body>
  <h3>Lista artisti</h3>
  <h4><a href="index.jsp">Torna alla Home</a></h4>
    
    <s:if test="error">
    	<h1>ERRORE</h1>
    </s:if>
    
    <s:if test="artistList.size > 0">
       <ol>
         <s:iterator value="artistList">
	  		<li>
	  			Nome:
	  			<s:property value="nickName" />
	  			<br/>
	  			
	  			Id artista:
	  			<s:property value="id" />
	  			<br/>
	  			
	  			<s:if test="albums.size > 0">
					Album dell'artista:
						<select multiple>
							<s:iterator value="albums">
								<option>
									<s:property value="name" />								
								</option>								
							</s:iterator>
						</select>	
						<br/>			
				</s:if>
				
				<s:if test="merch.size > 0">
					Merch dell'artista:
						<select multiple>
							<s:iterator value="merch">
								<option>
									<s:property value="title" />								
								</option>								
							</s:iterator>
						</select>	
						<br/>			
				</s:if>
				
  				<s:url action="editArtist" var="editUrl">
				       <s:param name="id" value="id"/>
			    </s:url>		
                <s:url action="deleteArtist" var="deleteUrl">
				       <s:param name="id" value="id"/>
			    </s:url>
			    
			    
	  			<a href="<s:property value='#editUrl' />" >Modifica</a>
	  			<br/>
	  			<a href="<s:property value='#deleteUrl' />" >Cancella</a>
	  			<br/><br/>
	  		</li>
		</s:iterator>
       </ol>
    </s:if>
   
	<s:url action="createArtist" var="newUrl">
       <s:param name="id" value="-1"/>
     </s:url>
     
    <p><a href="<s:property value='#newUrl' />" >Crea un nuovo artista.</a></p>
  </body>
</html>
