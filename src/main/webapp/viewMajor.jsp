<%@ page language="java" pageEncoding="ISO-8859-1" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">    
    <title>Vedi Major</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    

  </head>
  
  <body>
  <h3>Lista Major</h3>
  <h4><a href="index.jsp">Torna alla Home</a></h4>
    
    <s:if test="error">
    	<h1>ERRORE</h1>
    </s:if>
    
    <s:if test="majorList.size > 0">
       <ol>
         <s:iterator value="majorList">
	  		<li>
	  			Nome:
	  			<s:property value="name" />
	  			<br/>
	  			
	  			Id Major:
	  			<s:property value="id" />
	  			<br/>	  			
		
				<s:if test="artists.size > 0">
					Artisti nella major:
						<select multiple>
							<s:iterator value="artists">
								<option>
									<s:property value="nickName" />								
								</option>								
							</s:iterator>
						</select>	
						<br/>			
				</s:if>
				
				<s:if test="majorsOwned.size > 0">
					Major possedute:
						<select multiple>
							<s:iterator value="majorsOwned">
								<option>
									<s:property value="name" />								
								</option>								
							</s:iterator>
						</select>	
						<br/>			
				</s:if>
				<br/>
				
				<s:if test="ownedBy">
					Posseduta da:
		  			<s:property value="ownedBy.name" />
		  			<br/>
	  			</s:if>
				
		
				<s:url action="editMajor" var="editUrl">
				       <s:param name="id" value="id"/>
			    </s:url>		
                <s:url action="deleteMajor" var="deleteUrl">
				       <s:param name="id" value="id"/>
			    </s:url>

	  			<a href="<s:property value='#editUrl' />" >Modifica</a>
	  			<br/>
	  			<a href="<s:property value='#deleteUrl' />" >Cancella</a>
	  			<br/><br/>
	  		</li>
		</s:iterator>
       </ol>
    </s:if>
   
	<s:url action="createMajor" var="newUrl">
       <s:param name="id" value="-1"/>
     </s:url>
     
    <p><a href="<s:property value='#newUrl' />" >Crea una nuova major.</a></p>
  </body>
</html>
