<%@ page language="java" pageEncoding="ISO-8859-1" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>Gestisci prodotto</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    

  </head>
  
  <body>
  <s:if test="id > 0">
    <h3>Modifica prodotto</h3>
  </s:if>
  <s:else>
  	<h3>Crea prodotto</h3>
  
  </s:else>
  
  <s:form action='saveOrUpdateMerch'>
  
  	<p><s:textfield name="merch.title" label="Titolo" /> </p> <br />
  	<p><s:textfield name="merch.description" label="Descrizione" /> </p> <br />
  	<p><s:textfield name="merch.number" label="Numero prodotti in magazzino" /> </p> <br />


	<s:select label="Artista di appartenenza" name="idArtist" id="idArtist" 
      list="artistList"
      listKey="id"
      listValue="nickName"
      value="selectedArtist"/>
    
  	<s:hidden name="merch.id" />
  	<s:submit  value="Save" />
  
  </s:form>
   
  </body>
</html>
