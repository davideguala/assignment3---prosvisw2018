<%@ page language="java" pageEncoding="ISO-8859-1" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">    
    <title>Vedi merch</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    

  </head>
  
  <body>
  <h3>Lista merch</h3>
  <h4><a href="index.jsp">Torna alla Home</a></h4>
    
    <s:if test="error">
    	<h1>ERRORE</h1>
    </s:if>
    
    <s:if test="merchList.size > 0">
       <ol>
         <s:iterator value="merchList">
	  		<li>
	  		
	  			Titolo:
	  			<s:property value="title" />
	  			<br/>
	  			
	  			Descrizione:
	  			<s:property value="description" />
	  			<br/>
	  			
	  			Id merch:
	  			<s:property value="id" />
	  			<br/>
	  			
	  			Artista:
	  			<s:property value="artist" />
	  			<br/>	  			
	  			
  				<s:url action="editMerch" var="editUrl">
				       <s:param name="id" value="id"/>
			    </s:url>		
                <s:url action="deleteMerch" var="deleteUrl">
				       <s:param name="id" value="id"/>
			    </s:url>
			    
			    
	  			<a href="<s:property value='#editUrl' />" >Modifica</a>
	  			<br/>
	  			<a href="<s:property value='#deleteUrl' />" >Cancella</a>
	  			<br/><br/>
	  		</li>
		</s:iterator>
       </ol>
    </s:if>
   
	<s:url action="createMerch" var="newUrl">
       <s:param name="id" value="-1"/>
     </s:url>
     
    <p><a href="<s:property value='#newUrl' />" >Crea un nuovo prodotto.</a></p>
  </body>
</html>
