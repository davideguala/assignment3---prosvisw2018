package test;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import entities.Artist;
import entities.Major;
import entities.Merch;
import entities.Album;
import services.ArtistService;
import services.MajorService;
import services.MerchService;
import services.AlbumService;
import services.ServiceInterface;
import static org.junit.Assert.*;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.concurrent.Semaphore;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

//running the test in the order that i want
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ArtistTest {

	private static final ServiceInterface<Album> albumService = new AlbumService();
	private static final ServiceInterface<Major> majorService = new MajorService();
	private static final ServiceInterface<Artist> artistService = new ArtistService();
	private static final ServiceInterface<Merch> merchService = new MerchService();
	
	//using a semaphore in order to run tests in a synchronous way
	private static final Semaphore sem = new Semaphore(1);
	
	private Major major;
	private Artist artist1;
	private Artist artist2;
	private Artist artist3;
	private Album album1;
	private Album album2;
	private Album album3;
	private Merch merch1;
	private Merch merch2;
	
	//drop the db used for tests
	public static void dropDB() {
		final EntityManager entityManager = Persistence.createEntityManagerFactory("dbMusic").createEntityManager();
        entityManager.getTransaction().begin();
        entityManager.createNativeQuery("DELETE IGNORE FROM album_artist WHERE 1").executeUpdate();
        entityManager.createNativeQuery("DELETE IGNORE FROM " + Artist.tableName +" WHERE 1").executeUpdate();
        entityManager.createNativeQuery("DELETE IGNORE FROM " + Major.tableName + " WHERE 1").executeUpdate();
        entityManager.createNativeQuery("DELETE IGNORE FROM " + Album.tableName +" WHERE 1").executeUpdate();
        entityManager.createNativeQuery("DELETE IGNORE FROM " + Merch.tableName +" WHERE 1").executeUpdate();
        entityManager.getTransaction().commit();
        entityManager.close();
	}
	
	@BeforeClass
	public static void setUp() {
		dropDB();
		assertNotNull(majorService);
		assertNotNull(artistService);
		assertNotNull(albumService);
		assertNotNull(merchService);
		assertNotNull(sem);
	}
	
	@Test
    public void test1_add() throws Exception{
    	
    	sem.acquire();
    	
    	//set the albums and the major
    	album1 = new Album();
    	album1.setName("Status");
    	album1.setReleaseDate(new GregorianCalendar(2015, 1, 6).getTime());
    	
    	album2 = new Album();
    	album2.setName("Santeria");
    	album2.setReleaseDate(new GregorianCalendar(2016, 6, 6).getTime());
    	
    	album3 = new Album();
    	album3.setName("Fenomeno");
    	album3.setReleaseDate(new GregorianCalendar(2017, 4, 7).getTime());
    	
    	major = new Major();
		major.setName("Universal");
		major.setFoundation(new GregorianCalendar(1996, 12, 5).getTime());
		
		//set the artists
		
		artist1 = new Artist();
		artist2 = new Artist();
		artist3 = new Artist();
		assertNotNull(artist1);
		assertNotNull(artist2);
		assertNotNull(artist3);
		
    	artist1.setBirthDate(new GregorianCalendar(1980, 6, 8).getTime());
    	artist1.setNickName("Marracash");
    	artist1.setRealName("Fabio Rizzo");
    	major.getArtists().add(artist1);
    	
    	assertTrue(artist1.getBirthDate().before(new Date()));
    	assertEquals(artist1.getNickName(), "Marracash");
    	assertEquals(artist1.getRealName(), "Fabio Rizzo");
    	assertTrue(major.getArtists().contains(artist1));
    	
    	artist2.setBirthDate(new GregorianCalendar(1979, 12, 25).getTime());
    	artist2.setNickName("Gu� Pequeno");
    	artist2.setRealName("Cosimo Fini"); 	
    	
    	assertTrue(artist2.getBirthDate().before(artist1.getBirthDate()));
    	assertEquals(artist2.getNickName(), "Gu� Pequeno");
    	assertEquals(artist2.getRealName(), "Cosimo Fini");
    	assertFalse(major.getArtists().contains(artist2));
    	
    	artist3.setBirthDate(new GregorianCalendar(1976, 10, 17).getTime());
    	artist3.setNickName("Fabri Fibra");
    	artist3.setRealName("Fabrizio Tarducci");
    	major.getArtists().add(artist3);
    	
    	assertTrue(artist3.getBirthDate().before(artist2.getBirthDate()));
    	assertEquals(artist3.getNickName(), "Fabri Fibra");
    	assertEquals(artist3.getRealName(), "Fabrizio Tarducci");
    	assertTrue(major.getArtists().contains(artist3));
    	
    	album1.addArtist(artist1);
    	album2.addArtist(artist1);
    	album2.addArtist(artist2);
    	album3.addArtist(artist3);
    	
    	merch1 = new Merch();
    	merch1.setTitle("Felpa Status a righe");
    	merch1.setDescription("Bella felpa con descrizione lunga");
    	merch1.setNumber(4);
    	merch1.setArtist(artist1);
    	
    	merch2 = new Merch();
    	merch2.setTitle("Cappellino Zen");
    	merch2.setDescription("Cappello con descrizione corta");
    	merch2.setNumber(3);
    	merch2.setArtist(artist2);
    	
    	//save it all
    	artistService.add(artist1);
    	artistService.add(artist2);
    	artistService.add(artist3);
    	majorService.add(major);
    	albumService.add(album1);
    	albumService.add(album2);
    	albumService.add(album3);
    	merchService.add(merch1);
    	merchService.add(merch2);
    	
		sem.release(); 
    }
 
    @Test
    public void test2_read() throws Exception{
    	
    	sem.acquire();
    	
    	//READ artists
    	
    	//read all
    	List<Artist> allArtists = artistService.getAll();
    	
    	assertTrue(allArtists.size() == 3);
    	
    	artist1 = allArtists.get(0);
    	artist2 = allArtists.get(1);
    	artist3 = allArtists.get(2);
    	
    	assertEquals(artist1.getAlbums().size(), 2);
    	assertEquals(artist1.getMerch().size(), 1);
    	assertEquals(artist1.getNickName(), "Marracash");
		assertEquals(artist1.getRealName(), "Fabio Rizzo");
		assertTrue(artist1.getBirthDate().before(new Date()));
    	
		assertEquals(artist2.getAlbums().size(), 1);
		assertEquals(artist2.getMerch().size(), 1);
    	assertEquals(artist2.getNickName(), "Gu� Pequeno");
		assertEquals(artist2.getRealName(), "Cosimo Fini");
		assertTrue(artist2.getBirthDate().before(artist1.getBirthDate()));
		
		assertEquals(artist3.getAlbums().size(), 1);
		assertEquals(artist3.getMerch().size(), 0);    	
    	
    	assertEquals(artist3.getNickName(), "Fabri Fibra");
		assertEquals(artist3.getRealName(), "Fabrizio Tarducci");
		assertTrue(artist3.getBirthDate().before(artist2.getBirthDate()));
		
		//check merch
		
		List<Merch> allMerchs = merchService.getAll();
    	merch1 = allMerchs.get(0);
    	merch2 = allMerchs.get(1);
		
		assertEquals(merch1.getArtist().getRealName(), "Fabio Rizzo");
		assertNotSame(merch2.getArtist(), merch1.getArtist());
		assertEquals(merch2.getArtist().getRealName(), "Cosimo Fini");
		
    	//check album
    	album1 = artist1.getAlbums().get(0);
    	
    	assertEquals(album1.getName(), "Status");
    	assertEquals(album1.getArtists().size(), 1);
    	
    	album2 = albumService.get(album1.getId());
    	
    	assertEquals(album1, album2);
    	assertTrue(album2 == album1);
    	
    	//read single artist
    	Artist artist = artistService.get(artist1.getId());
    	
    	assertEquals(artist, artist1);
    	assertTrue(artist1 == artist);
    	
    	//search artist
    	List<Artist> artists = artistService.search("realName", "i");
    	assertTrue(artists.size() == 3);
    	
    	artist = artists.get(0);    	
    	assertEquals(artist, artist1);
    	assertTrue(artist1 == artist);
    	
    	artists = artistService.search("nickName", "ra");
    	assertTrue(artists.size() == 2);
    	
    	sem.release(); 
    }
    
    @Test
    public void test3_update() throws Exception{
    	sem.acquire();
    	
    	List<Artist> allArtists = artistService.getAll();
    	artist1 = allArtists.get(0);
    	artist2 = allArtists.get(1);
    	artist3 = allArtists.get(2);
    	
    	major = majorService.getAll().get(0);
    	
    	artist1.setRealName("Marracaaaash");
    	major.getArtists().add(artist2);
    	major.getArtists().remove(artist3);
    	
    	artistService.edit(artist1);
    	majorService.edit(major);
    	
    	//check
    	major = majorService.get(major.getId());
    	allArtists = artistService.getAll();
    	Artist artist4 = allArtists.get(0);
    	Artist artist5 = allArtists.get(1);
    	Artist artist6 = allArtists.get(2);
    	
    	assertEquals(artist1, artist4);
    	assertEquals(artist2, artist5);
    	assertEquals(artist3, artist6);   	

    	assertEquals(artist4.getRealName(), "Marracaaaash");
    	
    	//check major
    	assertTrue(major.getArtists().contains(artist5));
    	assertFalse(major.getArtists().contains(artist6));
    	
    	//check albums
    	assertEquals(artist4.getAlbums().size(), 2);    	
		assertEquals(artist5.getAlbums().size(), 1);		
		assertEquals(artist5.getAlbums().get(0).getName(), "Santeria");
		assertEquals(artist6.getAlbums().size(), 1);
		
    	sem.release(); 
    }
    
    @Test
    public void test4_delete() throws Exception{
    	
    	sem.acquire();
    	
    	List<Artist> allArtist = artistService.getAll();
    	artist1 = allArtist.get(0);
    	artist2 = allArtist.get(1);
    	artist3 = allArtist.get(2);
    	
    	//delete
    	artistService.delete(artist1);
    	
    	List<Album> albums = albumService.getAll();
    	assertEquals(albums.size(), 1);
    	
    	List<Major> allMajor = majorService.getAll();
    	major = allMajor.get(0);
    	assertEquals(major.getArtists().size(), 1);
    	
    	List<Merch> merch = merchService.getAll();
    	assertEquals(merch.size(), 1);
    	
    	artistService.delete(artist2);
    	
    	albums = albumService.getAll();
    	assertEquals(albums.size(), 1);
    	
    	allMajor = majorService.getAll();
    	major = allMajor.get(0);
    	assertEquals(major.getArtists().size(), 0);
    	
    	merch = merchService.getAll();
    	assertEquals(merch.size(), 0);
    	
    	artistService.delete(artist3);
    	
    	albums = albumService.getAll();
    	assertEquals(albums.size(), 0);
    	
    	allArtist = artistService.getAll();
    	assertTrue(allArtist.size() == 0);
    	
    	//check major
    	allMajor = majorService.getAll();
    	assertEquals(allMajor.size(), 1);

    	//check album
    	List<Album> allAlbum = albumService.getAll();
    	assertEquals(allAlbum.size(), 0);
    	
    	//check merch
    	merch = merchService.getAll();
    	assertEquals(merch.size(), 0);
    	
    	sem.release();
    }
	
	@AfterClass
    public static void after(){
    	dropDB();
    }
}
