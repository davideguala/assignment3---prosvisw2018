package test;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import entities.Artist;
import entities.Major;
import services.ArtistService;
import services.MajorService;
import services.ServiceInterface;
import static org.junit.Assert.*;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.concurrent.Semaphore;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

//running the test in the order that i want
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MajorTest{
	
	private static final ServiceInterface<Major> majorService = new MajorService();
	private static final ServiceInterface<Artist> artistService = new ArtistService();
	
	//using a semaphore in order to run tests in a synchronous way
	private static final Semaphore sem = new Semaphore(1);
	
	private Major major1;
	private Major major2;
	private Artist artist;
 
	//drop the db used for tests
	public static void dropDB() {
		final EntityManager entityManager = Persistence.createEntityManagerFactory("dbMusic").createEntityManager();
        entityManager.getTransaction().begin();
        entityManager.createNativeQuery("DELETE IGNORE FROM " + Artist.tableName +" WHERE 1").executeUpdate();
        entityManager.createNativeQuery("DELETE IGNORE FROM " + Major.tableName + " WHERE 1").executeUpdate();
        entityManager.getTransaction().commit();
        entityManager.close();
	}
	
	@BeforeClass
	public static void setUp() {
		dropDB();
		assertNotNull(majorService);
		assertNotNull(artistService);
		assertNotNull(sem);
	}
	
    @Test
    public void test1_add() throws Exception{
    	
    	sem.acquire();
    	
    	major1 = new Major();
    	major2 = new Major();
    	assertNotNull(major1);
		assertNotNull(major2);
		
		major1.setName("Universal");
		major1.setFoundation(new GregorianCalendar(1996, 12, 5).getTime());
		
		assertTrue(major1.getFoundation().before(new Date()));
		assertSame(major1.getName(), "Universal");
		
		major2.setName("Sony");
		major2.setFoundation(new GregorianCalendar(1845, 6, 8).getTime());

		assertTrue(major2.getFoundation().before(major1.getFoundation()));	
		assertTrue(major1.getArtists().size() == major2.getArtists().size());
    	
		//set one artist
		artist = new Artist();
    	artist.setBirthDate(new GregorianCalendar(1980, 6, 8).getTime());
    	artist.setNickName("Marracash");
    	artist.setRealName("Fabio Rizzo");
    	major1.getArtists().add(artist);
    	
		//save
    	artistService.add(artist);
		majorService.add(major1);
		majorService.add(major2);		
		
		sem.release(); 
    }
 
    @Test
    public void test2_read() throws Exception{
    	
    	sem.acquire();
    	
    	//READ majors
    	
    	//read all
    	List<Major> allMajor = majorService.getAll();
    	
    	assertTrue(allMajor.size() == 2);
    	
    	major1 = allMajor.get(0);
    	major2 = allMajor.get(1);
    	
    	assertSame(major1.getArtists().size(), 1);
    	assertSame(major1.getName(), "Universal");
    	assertTrue(major1.getFoundation().before(new Date()));
    	assertNull(major1.getOwnedBy());
    	assertSame(major1.getMajorsOwned().size(), 0);
    	
    	assertSame(major2.getArtists().size(), 0);
    	assertSame(major2.getName(), "Sony");
    	assertTrue(major2.getFoundation().before(major1.getFoundation()));
    	assertNull(major2.getOwnedBy());
    	assertSame(major2.getMajorsOwned().size(), 0);
    	
    	//check artist
    	artist = major1.getArtists().get(0);
    	
    	assertSame(artist.getNickName(), "Marracash");
    	
    	Artist artist1 = artistService.get(artist.getId());
    	
    	assertSame(artist1, artist);
    	assertTrue(artist == artist1);
    	
    	//read single major
    	Major major3 = majorService.get(major1.getId());
    	
    	assertSame(major3, major1);
    	assertTrue(major1 == major3);
    	
    	//search major
    	List<Major> majors = majorService.search("name", "vers");
    	assertTrue(majors.size() == 1);
    	
    	major3 = majors.get(0);    	
    	assertSame(major3, major1);
    	assertTrue(major1 == major3);
    	
    	majors = majorService.search("name", "n");
    	assertTrue(majors.size() == 2);
    	
    	sem.release(); 
    }
    
    @Test
    public void test3_update() throws Exception{
    	sem.acquire();
    	
    	List<Major> allMajor = majorService.getAll();
    	major1 = allMajor.get(0);
    	major2 = allMajor.get(1);
    	
    	major1.setName("New Universal");
    	major2.setFoundation(new GregorianCalendar(2015, 12, 12).getTime());
    	
    	major1.setOwnedBy(major2);
    	
    	majorService.edit(major1);
    	majorService.edit(major2);
    	
    	//check
    	allMajor = majorService.getAll();
    	Major major3 = allMajor.get(0);
    	Major major4 = allMajor.get(1);
    	
    	assertSame(major3.getName(), "New Universal");
    	assertSame(major3.getArtists().size(), 1);
    	assertTrue(major4.getFoundation().after(major3.getFoundation()));
    	assertSame(major4.getArtists().size(), 0);
    	
    	assertSame(major1.getOwnedBy(), major2);
    	assertSame(major1.getMajorsOwned().size(), 0);
    	
    	assertNull(major2.getOwnedBy());
    	assertSame(major2.getMajorsOwned().size(), 1);
    	
    	//check artist
    	artist = major3.getArtists().get(0);    	
    	assertSame(artist.getNickName(), "Marracash");    	
    	assertSame(artist, major1.getArtists().get(0));
    	
    	sem.release(); 
    }
    
    @Test
    public void test4_delete() throws Exception{
    	
    	sem.acquire();
    	
    	List<Major> allMajor = majorService.getAll();
    	major1 = allMajor.get(0);
    	major2 = allMajor.get(1);
    	
    	//delete
    	majorService.delete(major2);
    	
    	allMajor = majorService.getAll();
    	assertTrue(allMajor.size() == 1);
    	assertNull(allMajor.get(0).getOwnedBy());
    	
    	majorService.delete(major1);
    	
    	allMajor = majorService.getAll();
    	assertTrue(allMajor.size() == 0);
    	
    	sem.release(); 
    }
    
    @AfterClass
    public static void after(){
    	dropDB();
    }
 
}