package test;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import entities.Artist;
import entities.Merch;
import services.ArtistService;
import services.MerchService;
import services.ServiceInterface;
import static org.junit.Assert.*;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.concurrent.Semaphore;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

//running the test in the order that i want
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MerchTest {

	private static final ServiceInterface<Merch> merchService = new MerchService();
	private static final ServiceInterface<Artist> artistService = new ArtistService();
	
	//using a semaphore in order to run tests in a synchronous way
	private static final Semaphore sem = new Semaphore(1);

	private Artist artist1;
	private Artist artist2;
	private Merch merch1;
	private Merch merch2;
	private Merch merch3;
	
	//drop the db used for tests
		public static void dropDB() {
			final EntityManager entityManager = Persistence.createEntityManagerFactory("dbMusic").createEntityManager();
	        entityManager.getTransaction().begin();
	        entityManager.createNativeQuery("DELETE IGNORE FROM " + Artist.tableName +" WHERE 1").executeUpdate();
	        entityManager.createNativeQuery("DELETE IGNORE FROM " + Merch.tableName +" WHERE 1").executeUpdate();
	        entityManager.getTransaction().commit();
	        entityManager.close();
		}
		
		@BeforeClass
		public static void setUp() {
			dropDB();
			assertNotNull(artistService);
			assertNotNull(merchService);
			assertNotNull(sem);
		}
		
		@Test
	    public void test1_add() throws Exception{
	    	
	    	sem.acquire();
	    	
	    	//set artists
	    	artist1 = new Artist();			
	    	artist1.setBirthDate(new GregorianCalendar(1980, 6, 8).getTime());
	    	artist1.setNickName("Marracash");
	    	artist1.setRealName("Fabio Rizzo");
	    	
	    	artist2 = new Artist();
	    	artist2.setBirthDate(new GregorianCalendar(1979, 12, 25).getTime());
	    	artist2.setNickName("Gu� Pequeno");
	    	artist2.setRealName("Cosimo Fini"); 
	    	
	    	//set merchs
	    	merch1 = new Merch();
	    	merch2 = new Merch();
	    	merch3 = new Merch();
	    	assertNotNull(merch1);
			assertNotNull(merch2);
			assertNotNull(merch3);
			
	    	merch1.setTitle("Felpa Status a righe");
	    	merch1.setDescription("Bella felpa con descrizione lunga");
	    	merch1.setNumber(4);
	    	merch1.setArtist(artist1);
	    	
	    	assertEquals(merch1.getTitle(), "Felpa Status a righe");
	    	assertEquals(merch1.getDescription(), "Bella felpa con descrizione lunga");
	    	assertEquals(merch1.getArtist(), artist1);	    	
	    	
	    	merch2.setTitle("Cappellino Zen");
	    	merch2.setDescription("Cappello con descrizione corta");
	    	merch2.setNumber(3);
	    	merch2.setArtist(artist2);
	    	
	    	assertNotSame(merch1.getArtist(), merch2.getArtist());
	    	assertEquals(merch2.getTitle(), "Cappellino Zen");	
	    	assertTrue(merch2.getNumber() < merch1.getNumber());
	    	
	    	merch3.setTitle("Maglietta Vendetta");
	    	merch3.setDescription("Maglietta del tour del 2017");
	    	merch3.setNumber(1);
	    	merch3.setArtist(artist1);
	    	
	    	//save all
	    	artistService.add(artist1);
	    	artistService.add(artist2);
	    	merchService.add(merch1);
	    	merchService.add(merch2);
	    	merchService.add(merch3);
	    	
			sem.release(); 
	    }
	 
	    @Test
	    public void test2_read() throws Exception{
	    	
	    	sem.acquire();
	    	
	    	//READ artists
	    	
	    	//read all
	    	List<Merch> allMerchs = merchService.getAll();
	    	
	    	assertTrue(allMerchs.size() == 3);
	    	
	    	merch1 = allMerchs.get(0);
	    	merch2 = allMerchs.get(1);
	    	merch3 = allMerchs.get(2);
	    	
	    	artist1 = artistService.getAll().get(0);
	    	
	    	assertEquals(merch1.getTitle(), "Felpa Status a righe");
	    	assertEquals(merch1.getDescription(), "Bella felpa con descrizione lunga");
	    	assertEquals(merch1.getArtist(), artist1);	  
	    	
	    	assertNotSame(merch1.getArtist(), merch2.getArtist());
	    	assertEquals(merch2.getTitle(), "Cappellino Zen");	
	    	assertTrue(merch2.getNumber() < merch1.getNumber());
	    	
	    	assertEquals(merch3.getArtist(), merch1.getArtist());
	    	assertEquals(merch3.getTitle(), "Maglietta Vendetta");	
	    	assertTrue(merch3.getNumber() < merch1.getNumber());
			
			//check artists
			artist1 = merch1.getArtist();
			artist2 = merch2.getArtist();
			
			assertEquals(artist1.getRealName(), "Fabio Rizzo");
			assertEquals(merch3.getArtist().getRealName(), artist1.getRealName());
			assertNotSame(merch3.getArtist().getRealName(), artist2.getRealName());
			assertEquals(artist2.getRealName(), "Cosimo Fini");
	    	
	    	//read single merch
	    	Merch merch = merchService.get(merch1.getId());
	    	
	    	assertEquals(merch, merch1);
	    	assertTrue(merch1 == merch);
	    	
	    	//search merchs
	    	List<Merch> merchs = merchService.search("title", "ta");
	    	assertTrue(merchs.size() == 2);
	    	
	    	merch = merchs.get(0);    	
	    	assertEquals(merch, merch1);
	    	assertTrue(merch1 == merch);
	    	
	    	merchs = merchService.search("description", "a");
	    	assertTrue(merchs.size() == 3);
	    	
	    	sem.release(); 
	    }
	    
	    @Test
	    public void test3_update() throws Exception{
	    	sem.acquire();
	    	
	    	List<Merch> allMerchs = merchService.getAll();
	    	merch1 = allMerchs.get(0);
	    	merch2 = allMerchs.get(1);
	    	merch3 = allMerchs.get(2);
	    	
	    	artist2 = merch2.getArtist();
	    	
	    	merch1.setTitle("Felpa Status");	    	
	    	merch2.setArtist(merch1.getArtist());
	    	merch3.setArtist(artist2);
	    	
	    	merchService.edit(merch1);
	    	merchService.edit(merch2);
	    	merchService.edit(merch3);
	    	
	    	//check merchs
	    	allMerchs = merchService.getAll();
	    	Merch merch4 = allMerchs.get(0);
	    	Merch merch5 = allMerchs.get(1);
	    	Merch merch6 = allMerchs.get(2);
	    	
	    	assertEquals(merch1, merch4);
	    	assertEquals(merch2, merch5);
	    	assertEquals(merch3, merch6);   	

	    	assertEquals(merch4.getTitle(), "Felpa Status");
	    	
	    	//check artists
	    	assertEquals(merch4.getArtist().getNickName(), "Marracash");
	    	
	    	assertEquals(merch4.getArtist(), merch5.getArtist());
	    	
	    	assertEquals(merch6.getArtist().getNickName(), "Gu� Pequeno");	    	
	    	
	    
	    	sem.release(); 
	    }
	    
	    @Test
	    public void test4_delete() throws Exception{
	    	
	    	sem.acquire();
	    	
	    	List<Merch> allMerchs = merchService.getAll();
	    	merch1 = allMerchs.get(0);
	    	merch2 = allMerchs.get(1);
	    	merch3 = allMerchs.get(2);
	    	
	    	merchService.delete(merch1);
	    	
	    	allMerchs = merchService.getAll();
	    	assertEquals(allMerchs.size(), 2);
	    	
	    	merchService.delete(merch2);
	    	
	    	allMerchs = merchService.getAll();
	    	assertEquals(allMerchs.size(), 1);
	    	
	    	List<Artist> allArtist = artistService.getAll();
	    	
	    	artist1 = allArtist.get(0);
	    	assertEquals(artist1.getMerch().size(), 0);
	    	
	    	artist2 = allArtist.get(1);
	    	assertEquals(artist2.getMerch().size(), 1);
	    	
	    	merchService.delete(merch3);
	    	
	    	allMerchs = merchService.getAll();
	    	assertEquals(allMerchs.size(), 0);
	    	
	    	allArtist = artistService.getAll();
	    	assertEquals(allArtist.size(), 2);
	    	
	    	artist2 = allArtist.get(1);
	    	assertEquals(artist2.getMerch().size(), 0);
	    	
	    	sem.release(); 
	    }
		
		@AfterClass
	    public static void after(){
	    	dropDB();
	    }
}