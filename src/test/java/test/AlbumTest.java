package test;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import entities.Artist;
import entities.Album;
import services.ArtistService;
import services.AlbumService;
import services.ServiceInterface;
import static org.junit.Assert.*;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.concurrent.Semaphore;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

//running the test in the order that i want
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AlbumTest {

	private static final ServiceInterface<Album> albumService = new AlbumService();
	private static final ServiceInterface<Artist> artistService = new ArtistService();
	
	//using a semaphore in order to run tests in a synchronous way
	private static final Semaphore sem = new Semaphore(1);

	private Artist artist1;
	private Artist artist2;
	private Album album1;
	private Album album2;
	private Album album3;
	
	//drop the db used for tests
		public static void dropDB() {
			final EntityManager entityManager = Persistence.createEntityManagerFactory("dbMusic").createEntityManager();
	        entityManager.getTransaction().begin();
	        entityManager.createNativeQuery("DELETE IGNORE FROM " + Artist.tableName +" WHERE 1").executeUpdate();
	        entityManager.createNativeQuery("DELETE IGNORE FROM " + Album.tableName +" WHERE 1").executeUpdate();
	        entityManager.createNativeQuery("DELETE IGNORE FROM album_artist WHERE 1").executeUpdate();
	        entityManager.getTransaction().commit();
	        entityManager.close();
		}
		
		@BeforeClass
		public static void setUp() {
			dropDB();
			assertNotNull(artistService);
			assertNotNull(albumService);
			assertNotNull(sem);
		}
		
		@Test
	    public void test1_add() throws Exception{
	    	
	    	sem.acquire();
	    	
	    	//set artists
	    	artist1 = new Artist();			
	    	artist1.setBirthDate(new GregorianCalendar(1980, 6, 8).getTime());
	    	artist1.setNickName("Marracash");
	    	artist1.setRealName("Fabio Rizzo");
	    	
	    	artist2 = new Artist();
	    	artist2.setBirthDate(new GregorianCalendar(1979, 12, 25).getTime());
	    	artist2.setNickName("Gu� Pequeno");
	    	artist2.setRealName("Cosimo Fini"); 
	    	
	    	//set albums
	    	album1 = new Album();
	    	album2 = new Album();
	    	album3 = new Album();
	    	assertNotNull(album1);
			assertNotNull(album2);
			assertNotNull(album3);
	    	
	    	album1.setName("Status");
	    	album1.setReleaseDate(new GregorianCalendar(2015, 1, 6).getTime());
	    	album1.addArtist(artist1);
	    	
	    	assertTrue(album1.getReleaseDate().before(new Date()));
	    	assertEquals(album1.getName(), "Status");
	    	assertEquals(album1.getArtists().size(), 1);
	    	assertEquals(album1.getArtists().get(0), artist1);	    	
	    	
	    	album2.setName("Santeria");
	    	album2.setReleaseDate(new GregorianCalendar(2016, 6, 6).getTime());
	    	album2.addArtist(artist1);
	    	album2.addArtist(artist2);
	    	
	    	assertTrue(album2.getReleaseDate().after(album1.getReleaseDate()));
	    	assertEquals(album2.getName(), "Santeria");
	    	assertEquals(album2.getArtists().size(), 2);
	    	assertEquals(album2.getArtists().get(0), artist1);	
	    	assertEquals(album2.getArtists().get(1), artist2);
	    	
	    	album3.setName("Gentleman");
	    	album3.setReleaseDate(new GregorianCalendar(2018, 9, 15).getTime());
	    	album3.addArtist(artist2);
	    	
	    	assertTrue(album3.getReleaseDate().after(album2.getReleaseDate()));
	    	assertEquals(album3.getName(), "Gentleman");
	    	assertEquals(album3.getArtists().size(), 1);
	    	assertEquals(album3.getArtists().get(0), artist2);	
	    	
	    	//save all
	    	artistService.add(artist1);
	    	artistService.add(artist2);
	    	albumService.add(album1);
	    	albumService.add(album2);
	    	albumService.add(album3);
	    	
			sem.release(); 
	    }
	 
	    @Test
	    public void test2_read() throws Exception{
	    	
	    	sem.acquire();
	    	
	    	//READ artists
	    	
	    	//read all
	    	List<Album> allAlbums = albumService.getAll();
	    	
	    	assertTrue(allAlbums.size() == 3);
	    	
	    	album1 = allAlbums.get(0);
	    	album2 = allAlbums.get(1);
	    	album3 = allAlbums.get(2);
	    	
	    	assertEquals(album1.getArtists().size(), 1);
	    	assertEquals(album1.getName(), "Status");
			assertTrue(album1.getReleaseDate().before(new Date()));
	    	
			assertEquals(album2.getArtists().size(), 2);
	    	assertEquals(album2.getName(), "Santeria");
			assertTrue(album2.getReleaseDate().after(album1.getReleaseDate()));
			
			assertEquals(album3.getArtists().size(), 1);
	    	assertEquals(album3.getName(), "Gentleman");
			assertTrue(album3.getReleaseDate().after(album2.getReleaseDate()));
			
			//check artists
			artist1 = album1.getArtists().get(0);
			artist2 = album3.getArtists().get(0);
			
			assertEquals(artist1.getRealName(), "Fabio Rizzo");
			assertEquals(album2.getArtists().get(0).getRealName(), artist1.getRealName());
			assertEquals(album2.getArtists().get(1).getRealName(), artist2.getRealName());
			assertEquals(artist2.getRealName(), "Cosimo Fini");
	    	
	    	//read single album
	    	Album album = albumService.get(album1.getId());
	    	
	    	assertEquals(album, album1);
	    	assertTrue(album1 == album);
	    	
	    	//search albums
	    	List<Album> albums = albumService.search("name", "t");
	    	assertTrue(albums.size() == 3);
	    	
	    	album = albums.get(0);    	
	    	assertEquals(album, album1);
	    	assertTrue(album1 == album);
	    	
	    	albums = albumService.search("name", "nt");
	    	assertTrue(albums.size() == 2);
	    	
	    	sem.release(); 
	    }
	    
	    @Test
	    public void test3_update() throws Exception{
	    	sem.acquire();
	    	
	    	List<Album> allAlbums = albumService.getAll();
	    	album1 = allAlbums.get(0);
	    	album2 = allAlbums.get(1);
	    	album3 = allAlbums.get(2);
	    	
	    	artist1 = album1.getArtists().get(0);
	    	
	    	album1.setName("New Status");
	    	album2.removeArtist(artist1);
	    	album3.addArtist(artist1);
	    	
	    	albumService.edit(album1);
	    	albumService.edit(album2);
	    	albumService.edit(album3);
	    	
	    	//check albums
	    	allAlbums = albumService.getAll();
	    	Album album4 = allAlbums.get(0);
	    	Album album5 = allAlbums.get(1);
	    	Album album6 = allAlbums.get(2);
	    	
	    	assertEquals(album1, album4);
	    	assertEquals(album2, album5);
	    	assertEquals(album3, album6);   	

	    	assertEquals(album4.getName(), "New Status");
	    	
	    	//check artists
	    	assertEquals(album4.getArtists().size(), 1);
	    	assertEquals(album4.getArtists().get(0).getNickName(), "Marracash");
	    	
	    	assertEquals(album5.getArtists().size(), 1);
	    	assertEquals(album5.getArtists().get(0).getNickName(), "Gu� Pequeno");
	    	
	    	assertEquals(album6.getArtists().size(), 2);
	    	assertEquals(album6.getArtists().get(0).getNickName(), "Gu� Pequeno");
	    	assertEquals(album6.getArtists().get(1).getNickName(), "Marracash");
	    
	    	sem.release(); 
	    }
	    
	    @Test
	    public void test4_delete() throws Exception{
	    	
	    	sem.acquire();
	    	
	    	List<Album> allAlbums = albumService.getAll();
	    	album1 = allAlbums.get(0);
	    	album2 = allAlbums.get(1);
	    	album3 = allAlbums.get(2);
	    	
	    	albumService.delete(album1);
	    	
	    	allAlbums = albumService.getAll();
	    	assertEquals(allAlbums.size(), 2);
	    	
	    	albumService.delete(album2);
	    	
	    	allAlbums = albumService.getAll();
	    	assertEquals(allAlbums.size(), 1);
	    	
	    	albumService.delete(album3);
	    	
	    	allAlbums = albumService.getAll();
	    	assertEquals(allAlbums.size(), 0);
	    	
	    	List<Artist> allArtist = artistService.getAll();
	    	assertEquals(allArtist.size(), 2);
	    	
	    	sem.release(); 
	    }
		
		@AfterClass
	    public static void after(){
	    	dropDB();
	    }
}